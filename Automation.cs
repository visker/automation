﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;

namespace TDAI.Automation
{
    internal class Automation
    {
        #region 定义

        private readonly string front_url = "http://localhost:8000";
        private readonly string userName = "superAdmin";
        private readonly string password = "123456";
        private readonly string tableJs = "return document.getElementsByTagName('table')[0].rows.length";
        private readonly string completeJs = "alert('自动化 UI 测试完毕，整体页面未见异常，即将推出...')";

        private IWebDriver driver;
        private IJavaScriptExecutor js;

        #endregion

        #region 初始化

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver
            {
                Url = front_url
            };
            driver.Manage().Window.Maximize();
            js = (IJavaScriptExecutor)driver;
        }

        #endregion

        [Test]
        public void Automation_Browser()
        {
            #region 登录页面测试

            Thread.Sleep(500);
            var element = driver.FindElement(By.Id("username"));
            element.SendKeys(userName);

            Thread.Sleep(500);

            element = driver.FindElement(By.Id("password"));
            element.SendKeys(password);

            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("#formLogin>div.ant-row.ant-form-item>div>div>span>button"));
            element.Submit();

            Thread.Sleep(500);

            #endregion

            #region 工作台页面测试

            Assert.That(driver.PageSource, Does.Contain("工作台"));

            #endregion

            #region 在线用户页面测试

            element = driver.FindElement(By.LinkText("在线用户"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("在线用户"));
            Assert.That(driver.PageSource, Does.Contain("强制下线"));

            #endregion

            #region 系统监控页面测试

            element = driver.FindElement(By.LinkText("系统监控"));
            element.Click();

            Thread.Sleep(3000);

            element = driver.FindElement(By.CssSelector("#app>section>section>main>div>div.ant-row>div:nth-child(1)>div>div.ant-card-body>table>tr:nth-child(1)>td:nth-child(2)"));
            Assert.That(string.IsNullOrEmpty(element.Text), Is.False);
            element = driver.FindElement(By.CssSelector("#app>section>section>main>div>div.ant-card>div.ant-card-body>table>tr:nth-child(1)>td:nth-child(2)"));
            Assert.That(string.IsNullOrEmpty(element.Text), Is.False);

            #endregion

            #region 缓存列表页面测试

            element = driver.FindElement(By.LinkText("缓存列表"));
            element.Click();

            Thread.Sleep(500);

            var count = (long)js.ExecuteScript(tableJs);
            Assert.That(count, Is.GreaterThan(1));

            #endregion

            #region 系统管理点击事件

            element = driver.FindElement(By.CssSelector("#app > section > aside > div > ul > li:nth-child(2)".Trim()));
            element.Click();
            Thread.Sleep(500);

            #endregion

            #region 用户管理页面测试

            element = driver.FindElement(By.LinkText("用户管理"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("用户管理"));

            #endregion

            #region 角色管理页面测试

            element = driver.FindElement(By.LinkText("角色管理"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("角色管理"));

            #endregion

            #region 菜单管理页面测试

            element = driver.FindElement(By.LinkText("菜单管理"));
            element.Click();

            Thread.Sleep(500);

            count = (long)js.ExecuteScript(tableJs);
            Assert.That(count, Is.GreaterThan(1));

            #endregion

            #region 字典管理页面测试

            element = driver.FindElement(By.LinkText("字典管理"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("字典管理"));

            #endregion

            #region 组织管理页面测试

            element = driver.FindElement(By.LinkText("组织管理"));
            element.Click();

            Thread.Sleep(500);

            count = (long)js.ExecuteScript(tableJs);
            Assert.That(count, Is.GreaterThan(1));

            #endregion

            #region 登录日志页面测试

            element = driver.FindElement(By.LinkText("登录日志"));
            element.Click();

            Thread.Sleep(500);

            count = (long)js.ExecuteScript(tableJs);
            Assert.That(count, Is.GreaterThan(1));

            #endregion

            #region 操作日志页面测试

            element = driver.FindElement(By.LinkText("操作日志"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("操作日志"));

            #endregion

            #region 定时任务点击事件

            element = driver.FindElement(By.CssSelector("#app > section > aside > div > ul > li:nth-child(3)".Trim()));
            element.Click();
            Thread.Sleep(500);

            #endregion

            #region 定时任务页面测试

            element = driver.FindElement(By.LinkText("定时任务"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("定时任务"));

            #endregion

            #region 通知公告点击事件

            element = driver.FindElement(By.CssSelector("#app > section > aside > div > ul > li:nth-child(4)".Trim()));
            element.Click();
            Thread.Sleep(500);

            #endregion

            #region 通知管理页面测试

            element = driver.FindElement(By.LinkText("通知管理"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("通知管理"));

            #endregion

            #region 已收通知页面测试

            element = driver.FindElement(By.LinkText("已收通知"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("已收通知"));

            #endregion

            #region 流程管理点击事件

            element = driver.FindElement(By.CssSelector("#app > section > aside > div > ul > li:nth-child(5)".Trim()));
            element.Click();
            Thread.Sleep(500);

            #endregion

            #region 流程模板页面测试

            element = driver.FindElement(By.LinkText("流程模板"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("流程模板"));

            #endregion

            #region 业务流程页面测试

            element = driver.FindElement(By.LinkText("业务流程"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("业务流程"));

            #endregion

            #region 动态表单点击事件

            element = driver.FindElement(By.CssSelector("#app > section > aside > div > ul > li:nth-child(6)".Trim()));
            element.Click();
            Thread.Sleep(500);

            #endregion

            #region 表单设计页面测试

            element = driver.FindElement(By.LinkText("表单设计"));
            element.Click();

            Thread.Sleep(500);

            var baseComponent = driver.FindElement(By.CssSelector("#app > section > section > main > div > div > div > aside.left > div > div.ant-collapse-item.ant-collapse-item-active > div.ant-collapse-header".Trim()));
            Assert.That(baseComponent.Text, Is.EqualTo("基础控件"));

            #endregion

            #region 表单列表页面测试

            element = driver.FindElement(By.LinkText("表单列表"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("表单列表"));

            #endregion

            #region 案例点击事件

            element = driver.FindElement(By.CssSelector("#app > section > aside > div > ul > li:nth-child(7)".Trim()));
            element.Click();
            Thread.Sleep(500);

            #endregion

            #region 打印测试页面测试

            element = driver.FindElement(By.LinkText("打印测试"));
            element.Click();

            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("#app > section > section > main > div > div > div > button".Trim()));
            element.Click();

            Thread.Sleep(2000);
            driver.Navigate().Refresh();

            Thread.Sleep(2500);

            #endregion

            #region 可视化流程页面测试

            element = driver.FindElement(By.LinkText("可视化流程"));
            element.Click();

            Thread.Sleep(500);

            Assert.That(driver.PageSource, Does.Contain("可视化流程"));

            #endregion

            #region 表单示例页面测试

            element = driver.FindElement(By.LinkText("表单示例"));
            element.Click();

            Thread.Sleep(500);

            baseComponent = driver.FindElement(By.CssSelector("#app > section > section > main > div > div > div > div > div > aside.left > div > div.ant-collapse-item.ant-collapse-item-active > div.ant-collapse-header".Trim()));
            Assert.That(baseComponent.Text, Is.EqualTo("基础控件"));

            #endregion

            #region 引导页页面测试

            element = driver.FindElement(By.LinkText("引导页"));
            element.Click();

            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("#app > section > section > main > div > button".Trim()));
            element.Click();
            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-nextbutton".Trim()));
            element.Click();
            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("body > div.introjs-tooltipReferenceLayer.introjs-fixedTooltip > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-nextbutton".Trim()));
            element.Click();
            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("body > div.introjs-tooltipReferenceLayer.introjs-fixedTooltip > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-nextbutton".Trim()));
            element.Click();
            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("body > div.introjs-tooltipReferenceLayer.introjs-fixedTooltip > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-nextbutton".Trim()));
            element.Click();
            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-nextbutton".Trim()));
            element.Click();
            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("body > div.introjs-tooltipReferenceLayer > div > div.introjs-tooltipbuttons > a.introjs-button.introjs-nextbutton.introjs-donebutton".Trim()));
            element.Click();
            Thread.Sleep(500);

            #endregion

            #region 在线编辑页面测试

            element = driver.FindElement(By.LinkText("在线编辑"));
            element.Click();

            Thread.Sleep(1500);

            element = driver.FindElement(By.CssSelector("#app > section > section > main > div > div:nth-child(1) > select".Trim()));
            element.Click();

            Thread.Sleep(500);

            element = driver.FindElement(By.CssSelector("#app > section > section > main > div > div:nth-child(1) > select > option:nth-child(2)".Trim()));
            element.Click();

            element = driver.FindElement(By.CssSelector("#app > section > section > main > div > div:nth-child(1) > select".Trim()));
            element.Click();

            Thread.Sleep(2000);

            #endregion

            js.ExecuteScript(completeJs);

            Thread.Sleep(3000);

            Assert.Pass();
        }

        [TearDown]
        public void Close_Browser()
        {
            driver.Quit();
        }
    }
}
